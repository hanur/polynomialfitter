#include <array>
#include <cmath>

unsigned nLayers(const auto& hits)
{
    std::array<float, 6> zvals{};
    unsigned n = 0;
    for (const auto& hit: hits) {
        bool layerFound = false;
        for (unsigned i = 0; n != i; ++i) {
            if (std::abs(zvals[i] - hit.z()) < 3.f) {
                layerFound = true;
                break;
            }
        }
        if (layerFound) continue;
        if (n >= 6) throw "too many layers";
        zvals[n] = hit.z();
        n++;
    }
    return n;
}
